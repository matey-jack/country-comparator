# country-comparator

Some notes on the choice of tooling:
 - Snowpack for a nice and fast local dev experience
 - Snowpack also handles TypeScript and TSX via esbuild, so that adding 'typescript' 
   as dev dependency is purely for better editor-support!
 - @web/test-runner because Snowpack recommends it (implicitly comes with 'mocha')
   (note that apparently Snowpack offers more features than @web/dev-server, an otherwise similar project.)
 - also note that both test-runner and Snowpack use esbuild behind the scenes which should help with compatibility.
 - all files use ES-module style. Some example code from the documentation needed to be updated from module.exports and require() to using export/import keywords.
 - finally eslint
