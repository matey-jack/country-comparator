
Some notes on the choice of tooling:
- we started with Snowpack for a nice and fast local dev experience
- we also started with Yarn PnP for even faster package management
- but work great together!
- Snowpack also handles TypeScript and TSX via esbuild, so that adding 'typescript'
  as dev dependency is purely for better editor-support!
- eslint added and thrown out again because it auto-installed it's plugins via NPM and then didn't work OOTB.

now when it comes to testing, things become more complicated:
- the default to use with Yarn PnP would be Jest – but that requires either pre-building
  with 'tsc' or adding Babel as a dev dependency.
- It definitely did not want to have Babel, since there are already 'tsc' and 'esbuild' on the project.
- Snowpack actually recommends '@web/test-runner' (WTR) as testing framework and offers integration for that.
- And now the bummer: apparently WTR doesn't support Yarn PnP :-(
- So I just went back to NPM. Package managers are a thing of the past anyways ;-)

Now that there's NPM again, I could also try eslint again :shrug:

