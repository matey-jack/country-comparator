import {expect} from '@esm-bundle/chai';
import {zipOrderly} from './zipping';

const itemList = (list: number[]) =>
    list.map((value) => ({value}));

it('orders simply', () => {
    const a = itemList([1, 2, 3]);
    const b = itemList([1, 2.5, 4]);
    const result = zipOrderly([a, b]);
    expect(result.length).to.equal(3);
    result.forEach((row) => {
      expect(row.length).to.equal(2);
    });
});
