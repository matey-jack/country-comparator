
export interface ZipItem {
    value: number;
}

type Row = (null | ZipItem)[];

/**
 * @param inputs is a list of N ordered lists
 * @returns a list of Rows where each row has N items,
 * the i'th item of the row being either null or from the i'th input list,
 * such that all the ZipItems in row I have larger values than all the ZipItems in all the Rows before I,
 * and smaller than in all the rows after I.
 */
export function zipOrderly(inputs: ZipItem[][]): Row[] {
    return [];
}
