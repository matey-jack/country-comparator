
export interface CellData {
    name: string;
    value: number;
}

export interface AllData {
    [regionName: string]: CellData[];
}
