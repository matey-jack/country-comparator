import {useEffect, useState} from "react";

// "fire and forget" converter function to get rid of the Promise return type :shrug:
const fire = (f: () => any) => () => f();

export function useFetch(url: string, opts = {}) {
    const [result, setResult] = useState(null);
    const [loading, setLoading] = useState(false);
    const [hasError, setHasError] = useState(false);
    useEffect(fire(async () => {
        setLoading(true);
        try {
            const response = await fetch(url, opts);
            if (response.status !== 200) setHasError(true);
            const json = await response.json();
            console.log(json);
            setResult(json);
            setLoading(false);
        } catch (e) {
            setHasError(true);
            setLoading(false);
        }
    }), [url]);
    return {response: result, loading, hasError};
}

