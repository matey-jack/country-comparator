import React from 'react';
import {AllData, CellData} from './data';

export interface CellProps {
    data: CellData;
}

export const Cell = ({data}: CellProps) => <td>
    <span className="cell-name">{data.name}</span>
    <br/>
    <span className="cell-value">{data.value}</span>
</td>;


export interface RowProps {
    dataA: CellData;
    dataB: CellData;
}

export const Row = ({dataA, dataB}: RowProps) => <tr>
    <Cell data={dataA}/>
    <Cell data={dataB}/>
</tr>;

export interface TableProps {
    data: AllData
}

export function Table({data}: TableProps) {
    console.log(data);
    const columnNames = Object.keys(data);
    const columnA: CellData[] = data[columnNames[0]];
    const columnB: CellData[] = data[columnNames[1]];

    return (
        <table>
            <thead>
                <tr>
                    {columnNames.map((name) =>
                        <th key={name}>{name}</th>)
                    }
                </tr>
            </thead>
            <tbody>
                {columnA.map((dataA, i) =>
                    <Row dataA={dataA} dataB={columnB[i]} key={i}/>)
                }
            </tbody>
        </table>
    );
}
