import React from 'react';
import {useFetch} from './hooks'
import {Table} from "./Table";

export function App() {
    const data = useFetch('data.json');

    return (
        data.loading ? <div>Loading ...</div> :
            data.hasError || !data.response ? <div>Loading Error :-( </div> :
                <Table data={data.response}/>
    );
}
