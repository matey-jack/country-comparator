import { esbuildPlugin } from '@web/dev-server-esbuild';
import snowpackPlugin from '@snowpack/web-test-runner-plugin';

process.env.NODE_ENV = 'test';

export default {
  files: ['src/**/*.test.ts', 'src/**/*.spec.ts'],
  plugins: [
    esbuildPlugin({ ts: true }),
    snowpackPlugin()
  ],
};
